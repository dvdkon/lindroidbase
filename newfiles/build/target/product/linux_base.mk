# Product for building an Android build without any UI and a lot of other
# things to be used as a base for (GNU/ most likely)Linux in a LXC container.

PRODUCT_LOCALES := en_US

#A more minimal set of packages. It's missing a lot of useful things, like
#media playback HW acceleration libraries. Maybe put those in linux_full.mk?
PRODUCT_PACKAGES := \
	clatd \
	clatd.conf \
	pppd \
	libnfc_ndef \
	logd \
	libril \
	pppd \
	adbd \
	init \
	init.environ.rc \
	libEGL \
	libETC1 \
	libFFTEm \
	libGLESv1_CM \
	libGLESv2 \
	libbinder \
	libc \
	libcutils \
	libdl \
	libhardware \
	libhardware_legacy \
	liblog \
	libm \
	libstdc++ \
	libutils \
	linker \
	logcat \
	reboot \
	sh \
	toolbox \
	toybox \
	rild \
	debuggerd \
	libsigchain \
	property_contexts \
	lxc
#libsigchain is needed for ADB
#property_contexts is needed for init not to crash

PRODUCT_COPY_FILES += \
	linux/rootdir/init.linux-lxc.rc:root/init.linux-lxc.rc \
	linux/rootdir/init.linux-lxc.sh:root/init.linux-lxc.sh \
	linux/rootdir/system/etc/lxc/linux/config:system/etc/lxc/linux/config \
	system/core/rootdir/init.usb.rc:root/init.usb.rc \
	system/core/rootdir/init.usb.configfs.rc:root/init.usb.configfs.rc \
	system/core/rootdir/ueventd.rc:root/ueventd.rc \
	system/core/rootdir/etc/hosts:system/etc/hosts

ifeq ($(shell grep -c '^service adbd' system/core/rootdir/init.rc),0)
#Makefiles since Nougat have fewer services in the core init.rc file,
#so it seems it's no longer as necessary to modify it
#It's probably also not necessary to modify pre-Nougat rc files and not
#editing the Nougat one will cause a lot of runtime errors.
#This is something to consider. It's a tradeoff between having to edit
#config files and ensure they work with all Android versions and having
#a clean log
PRODUCT_COPY_FILES += system/core/rootdir/init.rc:root/init.rc
#$(PRODUCT_OUT)/root/init.rc:
files: modify-init-rc
modify-init-rc:
	if ! grep '^import /init.linux-lxc.rc$$' $(PRODUCT_OUT)/root/init.rc > /dev/null; \
	then echo 'import /init.linux-lxc.rc' >> $(PRODUCT_OUT)/root/init.rc; fi
else
PRODUCT_COPY_FILES += linux/rootdir/init.rc-pre-nougat:root/init.rc
endif

ifneq ($(wildcard system/core/rootdir/init.trace.rc),)
PRODUCT_COPY_FILES += \
	system/core/rootdir/init.trace.rc:root/init.trace.rc
endif

PRODUCT_PROPERTY_OVERRIDES += \
	ro.carrier=unknown

PRODUCT_BRAND := generic
PRODUCT_DEVICE := generic
PRODUCT_NAME := generic_linux_base

