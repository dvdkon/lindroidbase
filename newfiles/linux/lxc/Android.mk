# TODO: Support for archituectures other than ARM (probably aarch64)

#Ugly hack, just like the rest of the Android build system
LOCAL_PATH_LXC := linux/lxc
ANDROID_ROOT := ../..
TARGET_TOOLCHAIN_ROOT := prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.9
PRODUCT_OUT := out/target/product/mako
LXC_VERSION := 2.0.7
#TODO: Better directory choice. Maybe there's a make function?
BIN_OVERRIDE_DIR := out/host/common/lxc_bin_override

$(LOCAL_PATH_LXC)/lxc-$(LXC_VERSION):
	cd $(LOCAL_PATH_LXC)/; \
	if [[ ! -d "lxc-$(LXC_VERSION)" ]]; then \
		wget https://linuxcontainers.org/downloads/lxc/lxc-$(LXC_VERSION).tar.gz; \
		tar xf lxc-$(LXC_VERSION).tar.gz; \
		rm lxc-$(LXC_VERSION).tar.gz; \
	fi;

$(LOCAL_PATH_LXC)/libcap-source:
	if [[ ! -d "$(LOCAL_PATH_LXC)/libcap-source" ]]; then \
		git clone 'https://git.kernel.org/pub/scm/linux/kernel/git/morgan/libcap.git' "$(LOCAL_PATH_LXC)/libcap-source" \
	else \
		cd "$(LOCAL_PATH_LXC)/libcap-source"; \
		git pull; \
	fi;

#I can't find a way to pass "-pie -fPIE" to all gcc and ld calls using lxc's
#build system, so I used this hack
$(BIN_OVERRIDE_DIR):
	echo -e '#!/usr/bin/env sh\nexec '"$$PWD"'/$(TARGET_TOOLCHAIN_ROOT)/bin/arm-linux-androideabi-gcc -pie -fPIE $$*' > $(BIN_OVERRIDE_DIR)/arm-linux-androideabi-gcc
	chmod +x $(BIN_OVERRIDE_DIR)/arm-linux-androideabi-gcc

$(LOCAL_PATH_LXC)/libcap-out: $(LOCAL_PATH_LXC)/libcap-source
	export PATH=$$PWD/$(BIN_OVERRIDE_DIR):$$PWD/$(TARGET_TOOLCHAIN_ROOT)/bin:$$PATH; \
	cd $(LOCAL_PATH_LXC); \
	cd libcap-source; \
	make clean; \
	make CC="arm-linux-androideabi-gcc --sysroot $$PWD/../$(ANDROID_ROOT)/prebuilts/ndk/current/platforms/android-21/arch-arm" BUILD_CC=gcc PAM_CAP=no PROGS=; \
	cd ..; \
	mkdir -p libcap-out/{,lib}; \
	cp -r libcap-source/libcap/include libcap-out/; \
	cp -r libcap-source/libcap/{libcap.so.*,libcap.a} libcap-out/lib

# TODO: Deal with the "$(ANDROID_ROOT)/"
$(LOCAL_PATH_LXC)/lxc-$(LXC_VERSION)/src/config.h: $(LOCAL_PATH_LXC)/lxc-$(LXC_VERSION) $(BIN_OVERRIDE_DIR) $(LOCAL_PATH_LXC)/libcap-out
	export PATH=$$PWD/$(BIN_OVERRIDE_DIR):$$PWD/$(TARGET_TOOLCHAIN_ROOT)/bin:$$PATH; \
	cd $(LOCAL_PATH_LXC)/lxc-$(LXC_VERSION); \
	./configure \
		--host=arm-linux-androideabi \
		--with-sysroot="$$PWD/../$(ANDROID_ROOT)/prebuilts/ndk/current/platforms/android-21/arch-arm" \
		LDFLAGS="--sysroot $$PWD/../$(ANDROID_ROOT)/prebuilts/ndk/current/platforms/android-21/arch-arm -L$$PWD/../libcap-out/lib -lcap" \
		CFLAGS="--sysroot $$PWD/../$(ANDROID_ROOT)/prebuilts/ndk/current/platforms/android-21/arch-arm" \
		CPP=arm-linux-androideabi-cpp \
		CPPFLAGS="-isystem $$PWD/../$(ANDROID_ROOT)/prebuilts/ndk/current/platforms/android-21/arch-arm/usr/include -I$$PWD/../libcap-out/include" \
		--with-distro=debian \
		--prefix=/system/ \
		--with-runtime-path=/cache/ \
		--with-config-path=/system/etc/lxc \
		--disable-api-docs \
		--disable-lua \
		--disable-python \
		--disable-examples

$(PRODUCT_OUT)/system/lxc: $(LOCAL_PATH_LXC)/lxc-$(LXC_VERSION)/src/config.h $(BIN_OVERRIDE_DIR)
	cd $(LOCAL_PATH_LXC)/lxc-$(LXC_VERSION); \
	export PATH=$$PWD/$(BIN_OVERRIDE_DIR):$$PWD/$(TARGET_TOOLCHAIN_ROOT)/bin:$$PATH; \
	make; \
	make install DESTDIR="$$PWD/../$(ANDROID_ROOT)/$(PRODUCT_OUT)/"; \
	cd -; \
	rm -r $(BIN_OVERRIDE_DIR)

lxc: $(PRODUCT_OUT)/system/lxc

#$(CLEAR_VARS)

