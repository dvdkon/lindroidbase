Contents of `lxc-bin` will be copied to the root filesystem. LXC binaries
should be in `/data/lxc/lxc/bin`. This location might change, as it's only that
because builds from [https://jenkins.linuxcontainers.org/view/LXC/view/LXC
builds/job/lxc-build-android](the official Jenkins CI) use it. Maybe LXC will
even be built by the Android build system one day.

The Linux rootfs should be in `root.tar.gz`.
