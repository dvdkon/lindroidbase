#!/system/bin/sh
rootpath="$(cat /data/linux/rootpath)"
if [ "$rootpath" != /data/linux/root ]
then
	mount --bind "$rootpath" /data/linux/root
fi
lxc-start -n linux -F

