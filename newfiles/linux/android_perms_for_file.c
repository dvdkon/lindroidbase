#include <stdio.h>
#include <string.h>

#include <private/android_filesystem_config.h>

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Usage: %s <file path relative to Android root>\n"
			"The file path needs to end in a slash if it's a directory\n"
			"Returns: <octal permissions> <owner ID>:<group ID>\n",
			argv[0]);
		return 1;
	}

	char *path = argv[1];
	int is_dir = path[strlen(path)] == '/';

	unsigned int uid, gid, mode;
	//What's this for? Let's just ignore it for now.
	uint64_t capabilities; char target_out_path[256];

	fs_config(path, is_dir, target_out_path, &uid, &gid, &mode, &capabilities);

	//For some reason, mode seems to be more than 4 octal digits
	//for example, 70755 instead of just 0755. This & is also done all over
	//the Android code that uses this function, so it;s probably fine
	//(if mysterious and ugly)

	mode &= 07777;

	printf("%o %i:%i\n", mode, uid, gid);
}

